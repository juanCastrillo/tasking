package com.juan.tasking

import android.app.Application
import com.juan.tasking.repositories.TasksRepository
import com.juan.tasking.storage.databases.TasksDataBase

class Start: Application(){
    lateinit var tasksRepository: TasksRepository
    lateinit var tasksDataBase:TasksDataBase

    override fun onCreate() {
        super.onCreate()

        tasksDataBase = TasksDataBase.getInstance(this)
        tasksRepository = TasksRepository.getInstance(tasksDataBase.tasksDao())

    }
}