package com.juan.tasking.extras

import android.content.res.Resources
import android.util.Log
import androidx.core.content.ContextCompat
import com.juan.tasking.R
import java.time.DayOfWeek

fun DayOfWeek.toInt() =
    when(this){
        DayOfWeek.MONDAY -> 0
        DayOfWeek.TUESDAY -> 1
        DayOfWeek.WEDNESDAY -> 2
        DayOfWeek.THURSDAY -> 3
        DayOfWeek.FRIDAY -> 4
        DayOfWeek.SATURDAY -> 5
        DayOfWeek.SUNDAY -> 6
    }
fun createDay(i:Int) =
    when(i){
        0 -> DayOfWeek.MONDAY
        1 -> DayOfWeek.TUESDAY
        2 -> DayOfWeek.WEDNESDAY
        3 -> DayOfWeek.THURSDAY
        4 -> DayOfWeek.FRIDAY
        5 -> DayOfWeek.SATURDAY
        6 -> DayOfWeek.SUNDAY
        else -> DayOfWeek.MONDAY
    }


fun List<DayOfWeek>.concatenate():String {

    var list = ""

    this.forEach { list += it.toInt() }

    return list
}

/**
 * @param daysList a String with numbers concatenated each number representing a DayOfWeek object
 * Each character of the string is separated converted to DayOfWeek
 * and added to a list of DayOfWeek
 */
fun concatenationToListDayWeek(daysList:String):List<DayOfWeek> {

    val list = mutableListOf<DayOfWeek>()

    val base = '0'.toInt()

    daysList.forEach { list.add(createDay(it.toInt() - base)) }

    return list
}