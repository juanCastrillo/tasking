package com.juan.tasking.extras

import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.annotation.Dimension
import com.juan.tasking.R
import com.juan.tasking.models.FullTask
import com.juan.tasking.storage.entities.Task
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit



fun LocalTime.toDoubleX():Long {

    val hours = this.hour * 3600
    val minutesInHout = this.minute * 60
    val secondsInHour = this.second

    Log.d("TimeOutput", "$hours:$minutesInHout:$secondsInHour")

    return hours+minutesInHout+secondsInHour.toLong()
}

val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
val formatter2 = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")

val readableDateFormatter = DateTimeFormatter.ofPattern("EEEE', 'dd MMMM yyyy")
//val readableDateFormatter = DateTimeFormatter.RFC_1123_DATE_TIME

fun Task.toFullTask()= FullTask(this, null)
fun List<Task>.toFullTask():List<FullTask> {
    val fulltasks = mutableListOf<FullTask>()
    this.forEach { fulltasks.add(FullTask(it, null)) }
    return fulltasks
}

//fun doAsync(lambda:() -> (Unit)){
//    GlobalScope.launch(Dispatchers.Default) { lambda }
//}

fun secondsPast(start:LocalDateTime, stop:LocalDateTime= LocalDateTime.now()) = ChronoUnit.SECONDS.between(start, stop)

fun getDefaultImage(thing:String){

}

fun cornerPixelPosition(width: Float){
    width
}

fun getDisplayPixels(){

}

class Utils{
    companion object {
        fun runOnUiThread(runnable: Runnable) {
            val uiHandler = Handler(Looper.getMainLooper())
            uiHandler.post(runnable)
        }
    }
}
