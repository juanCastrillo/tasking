package com.juan.tasking.models

import androidx.room.Embedded
import com.juan.tasking.storage.entities.DayWork
import com.juan.tasking.storage.entities.Task
import java.time.LocalTime

data class FullTask(

    @Embedded var task: Task,
    @Embedded var dayWork: DayWork?

)