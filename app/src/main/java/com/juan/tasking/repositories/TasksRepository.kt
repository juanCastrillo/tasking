package com.juan.tasking.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import com.juan.tasking.models.FullTask
import com.juan.tasking.storage.daos.TasksDao
import com.juan.tasking.storage.entities.DayWork
import com.juan.tasking.storage.entities.DaysConverters
import com.juan.tasking.storage.entities.LocalDateConverters
import com.juan.tasking.storage.entities.Task
import java.time.DayOfWeek
import java.time.LocalDate

class TasksRepository(val tasksDao:TasksDao) {

    fun getTask(id:Long) = tasksDao.getTask(id)

    fun getDayTask(date: LocalDate):LiveData<List<FullTask>> {
        Log.d("query", "SELECT * FROM tasks LEFT OUTER JOIN dayswork on tasks.task_id = dayswork.ext_task_id WHERE days LIKE '%${DaysConverters.DaysConverter(listOf(date.dayOfWeek))}%' AND (date like '%${LocalDateConverters.LocalDatetoString(date)}%' OR date is NULL)")
        return tasksDao.getDayTasks(listOf(date.dayOfWeek), date)
    }

    fun getWeekTasks(week:String){}

    fun getAllTasks() = tasksDao.getAll()
    fun addTask(task: Task) {Log.d("insertTask", task.toString())
        tasksDao.insert(task)}

    fun updateProgress(dayWork:DayWork) = tasksDao.updateProgress(dayWork)



    companion object {

        private var sInstance: TasksRepository? = null

        fun getInstance(tasksDao: TasksDao): TasksRepository {
            if (sInstance == null) {
                sInstance = TasksRepository( tasksDao)
            }
            return sInstance!!
        }
    }

}