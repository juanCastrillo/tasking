package com.juan.tasking.storage.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.juan.tasking.models.FullTask
import com.juan.tasking.storage.entities.DayWork
import com.juan.tasking.storage.entities.Task
import java.time.DayOfWeek
import java.time.LocalDate

@Dao
interface TasksDao {

    @Query("SELECT * FROM tasks")
    fun getAll():LiveData<List<Task>>

    @Query("SELECT * FROM tasks LEFT OUTER JOIN dayswork ON task_id = ext_task_id WHERE task_id = :id order by date limit 1")
    fun getTask(id:Long): LiveData<FullTask>

    @Insert(onConflict = REPLACE)
    fun insert(task:Task)

    @Insert(onConflict = REPLACE)
    fun updateProgress(dayWork: DayWork)

    @Query("SELECT * FROM tasks LEFT OUTER JOIN dayswork on tasks.task_id = dayswork.ext_task_id WHERE days LIKE '%'||:days||'%' AND (date like '%'||:date||'%' OR date is NULL)")
    fun getDayTasks(days: List<DayOfWeek>, date:LocalDate):LiveData<List<FullTask>>

}