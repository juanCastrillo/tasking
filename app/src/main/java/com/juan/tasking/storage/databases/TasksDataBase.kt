package com.juan.tasking.storage.databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.juan.tasking.storage.daos.TasksDao
import com.juan.tasking.storage.entities.*

@Database(entities = [Task::class, DayWork::class], version = 1, exportSchema = false)
@TypeConverters(DaysConverters::class, LocalDateConverters::class, LocalDateTimeConverters::class)
abstract class TasksDataBase: RoomDatabase() {

    abstract fun tasksDao(): TasksDao

    companion object {
        private var INSTANCE: TasksDataBase? = null

        fun getInstance(ct: Context): TasksDataBase {
            if (INSTANCE == null) {
                synchronized(TasksDataBase::class.java) {
                    INSTANCE = Room.databaseBuilder(
                        ct.applicationContext, TasksDataBase::class.java, "tasks.db"
                    ).build()//.addMigrations(MIGRATION_1_2).build()
                }
            }
            return INSTANCE!!
        }

        fun destroyInstance(){ INSTANCE = null }
    }
}
