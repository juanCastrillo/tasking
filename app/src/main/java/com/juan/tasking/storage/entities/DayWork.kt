package com.juan.tasking.storage.entities

import androidx.room.*
import com.juan.tasking.extras.formatter
import com.juan.tasking.extras.formatter2
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime

@Entity(tableName = "dayswork")
data class DayWork (
    @PrimaryKey
    @ColumnInfo(name = "dayswork_id")
    val id:Long?,
    var date: LocalDate?,
    var lastTime: LocalDateTime?,
    @ForeignKey(childColumns = ["ext_task_id"], entity = Task::class, parentColumns = ["task_id"])
    var ext_task_id: Long,
    var progress: Long, //number of seconds doing the task
    var isNecessary: Boolean //is the task done in a required day
) {constructor():this(null, null, null,0, 0, false)}


class LocalDateConverters {

    companion object {

        @TypeConverter
        @JvmStatic
        fun LocalDatetoString(date: LocalDate): String = formatter.format(date)

        @TypeConverter
        @JvmStatic
        fun StringtoLocalDate(dateString: String): LocalDate = LocalDate.parse(dateString, formatter)
    }
}

class LocalDateTimeConverters {
    companion object {
        @TypeConverter
        @JvmStatic
        fun LocalDateTimetoString(date: LocalDateTime?): String? = if(date == null) null else formatter2.format(date)

        @TypeConverter
        @JvmStatic
        fun StringtoLocalDateTime(dateString: String?): LocalDateTime? = if(dateString == null) null else LocalDateTime.parse(dateString, formatter2)
    }
}