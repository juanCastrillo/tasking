package com.juan.tasking.storage.entities

import android.util.Log
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.juan.tasking.extras.concatenate
import com.juan.tasking.extras.concatenationToListDayWeek
import com.juan.tasking.extras.createDay
import com.juan.tasking.extras.toInt
import java.time.DayOfWeek

/**
 * Task to be acomplished
 */
@Entity(tableName = "tasks")
data class Task(

    @PrimaryKey
    @ColumnInfo(name = "task_id")
    val id:Long?,
    var title:String,           //title of the task
    var description:String,     //description of the task
    var duration:Long,        //ammount of seconds the task last/day
    var days:List<DayOfWeek>    //list of the days of the week this task should be done

) { constructor():this(null, "", "",0, emptyList()) }

/**
 * Class containing methods to allow room to save
 * lists of DayOfWeek Objects as String concatenations
 * being each day of the week starting from Monday -> 0 to Sunday -> 6
 */
class DaysConverters{
    companion object {

        /**
         * @param days a list of DayOfWeek objects
         * changes each day to an int and concatenates it to a string
         * of all the days specified
         */
        @TypeConverter
        @JvmStatic
        fun DaysConverter(days: List<DayOfWeek>): String = days.concatenate()


        /**
         * @param daysList a String with numbers concatenated each number representing a DayOfWeek object
         * @return list of DayOfWeek object
         */
        @TypeConverter
        @JvmStatic
        fun DaysReverter(daysList: String): List<DayOfWeek> = concatenationToListDayWeek(daysList)
    }
}

