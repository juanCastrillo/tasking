package com.juan.tasking.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.juan.tasking.models.FullTask
import com.juan.tasking.repositories.TasksRepository
import com.juan.tasking.storage.entities.DayWork
import com.juan.tasking.storage.entities.Task
import java.time.DayOfWeek
import java.time.LocalDate
import java.util.*
import kotlin.concurrent.thread

class TaskViewModel(application: Application, val tasksRepository:TasksRepository): AndroidViewModel(application){

    fun getTodayTasks():LiveData<List<FullTask>> = tasksRepository.getDayTask(LocalDate.now())

    fun getTask(id:Long) = tasksRepository.getTask(id)

    fun getAllTasks() = tasksRepository.getAllTasks()

    fun addTask(task:Task) = tasksRepository.addTask(task)

    fun getPercentage(task: FullTask, progress:Long):Float {
        //if(task.dayWork == null) return 0f
        val max = task.task.duration
        val current = progress
        val percentage = if(max == 0L) 1f else current/max.toFloat()
        return percentage
    }

    fun updateProgress(dayWork:DayWork) {tasksRepository.updateProgress(dayWork)}

    fun isNeccesaryTask(task:FullTask, day:DayOfWeek):Boolean {

        task.task.days.forEach {
            if(it == day) return true
        }

        return false
    }
}

/**
 * Factory to create the viewmodel
 */
class Factory(private val tasksRepository: TasksRepository, val application: Application) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        return TaskViewModel(application, tasksRepository) as T
    }
}