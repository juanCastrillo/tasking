package com.juan.tasking.views

import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.widget.EditText
import android.widget.NumberPicker
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.juan.tasking.R
import com.juan.tasking.Start
import com.juan.tasking.extras.createDay
import com.juan.tasking.extras.toDoubleX
import com.juan.tasking.storage.entities.Task
import com.juan.tasking.viewmodels.Factory
import com.juan.tasking.viewmodels.TaskViewModel
import kotlinx.android.synthetic.main.activity_create_task.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.time.DayOfWeek
import java.time.Duration
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.temporal.Temporal
import java.time.temporal.TemporalAmount
import java.time.temporal.TemporalField
import java.time.temporal.TemporalUnit

/**
 * activity to create a Task
 *
 * Select your task name and description
 * Which days of the week you want to do it
 * How many hours everyday
 */
class CreateTaskActivity:AppCompatActivity() {

    private lateinit var taskTitleEdit:TextInputEditText
    private lateinit var taskDescriptionEdit:TextInputEditText

    private lateinit var daysOfWeekRecyclerView:RecyclerView
    private lateinit var daysOfWeekAdapter: DaysOfWeekAdapter

    private lateinit var timeRemaining:TextView

    private lateinit var hoursPicker: NumberPicker
    private lateinit var minutesPicker:NumberPicker
    private lateinit var secondsPicker: NumberPicker

    private lateinit var createTaskButton: MaterialButton

    private lateinit var viewModel:TaskViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_task)

        viewModel = ViewModelProviders.of(this, Factory((application as Start).tasksRepository, application)).get(TaskViewModel::class.java)

        taskTitleEdit = findViewById(R.id.taskTitleEditText)
        taskDescriptionEdit = findViewById(R.id.taskDescriptionEditText)

        daysOfWeekRecyclerView = findViewById(R.id.daysOfWeekList)

        hoursPicker = findViewById(R.id.hourPicker)
        minutesPicker = findViewById(R.id.minutePicker)
        secondsPicker = findViewById(R.id.secondPicker)

        hoursPicker.apply {
            minValue = 0
            maxValue = 24
        }

        minutesPicker.apply {
            minValue = 0
            maxValue = 60
        }

        secondsPicker.apply {
            minValue = 0
            maxValue = 60
        }

        /*hoursPicker.displayedValues = Array<String>(size = 25, init = {i ->  i.toString()})
        minutesPicker.displayedValues = Array<String>(size = 61, init = {i ->  i.toString()})
        secondsPicker.displayedValues = Array<String>(size = 61, init = {i ->  i.toString()})*/

        createTaskButton = findViewById(R.id.createTaskButton2)

        val dayList = listOf<String>(
            resources.getString(R.string.monday),
            resources.getString(R.string.tuesday),
            resources.getString(R.string.wednesday),
            resources.getString(R.string.thursday),
            resources.getString(R.string.friday),
            resources.getString(R.string.saturday),
            resources.getString(R.string.sunday)

        )

        daysOfWeekAdapter = DaysOfWeekAdapter(dayList)
        daysOfWeekRecyclerView.adapter = daysOfWeekAdapter
        daysOfWeekRecyclerView.layoutManager = LinearLayoutManager(this)

        createTaskButton.setOnClickListener{
            val daysSelected = mutableListOf<DayOfWeek>()
            for(i in 0 until dayList.size){
                if(daysOfWeekAdapter.clickedList[i]) daysSelected.add(createDay(i))
            }
            /*Log.d("CreateTaskActivity", "clicksList: ${daysOfWeekAdapter.clickedList}")
            Log.d("CreateTaskActivity", "daysList: $daysSelected")*/

            val duration = LocalTime.of(hoursPicker.value, minutesPicker.value, secondsPicker.value).toDoubleX()


            Log.d("CreateTaskActivity", "pickers: ${hoursPicker.value}:${minutesPicker.value}:${secondsPicker.value}")
            Log.d("CreateTaskActivity", "duration: $duration")

            GlobalScope.launch(Dispatchers.Default) {
                viewModel.addTask(
                    Task(
                        id = null,
                        title = taskTitleEdit.text.toString(),
                        description = taskDescriptionEdit.text.toString(),
                        duration = duration,
                        days = daysSelected
                    )
                )
            }

            finish()
        }

    }
}