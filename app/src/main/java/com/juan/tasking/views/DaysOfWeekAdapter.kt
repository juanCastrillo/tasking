package com.juan.tasking.views

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.juan.tasking.R
import kotlinx.android.synthetic.main.dayofweek_card.view.*
import java.time.DayOfWeek

class DaysOfWeekAdapter(private val daysList:List<String>): RecyclerView.Adapter<DaysOfWeekAdapter.DaysOfWeekViewHolder>() {

    val clickedList = MutableList(daysList.size) {false}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DaysOfWeekViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.dayofweek_card, parent, false)
        return DaysOfWeekViewHolder(view)
    }

    override fun getItemCount() = daysList.size

    override fun onBindViewHolder(holder: DaysOfWeekViewHolder, position: Int) {
        holder.dayofweekCard.setOnClickListener{
            if(!clickedList[position])
                holder.dayofweekCard.setBackgroundColor(Color.BLUE)
            else holder.dayofweekCard.setBackgroundColor(Color.WHITE)
            clickedList[position] = !clickedList[position]
        }
        holder.dayText.text = daysList[position]
    }

    class DaysOfWeekViewHolder(v: View): RecyclerView.ViewHolder(v) {

        val dayofweekCard:View
        val dayText: TextView

        init {
            dayofweekCard = v.findViewById(R.id.dayofweek_card)
            dayText = v.findViewById(R.id.dayText)
        }
    }
}