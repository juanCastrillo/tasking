package com.juan.tasking.views

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.button.MaterialButton
import com.juan.tasking.R
import com.juan.tasking.Start
import com.juan.tasking.extras.readableDateFormatter
import com.juan.tasking.extras.toFullTask
import com.juan.tasking.viewmodels.Factory
import com.juan.tasking.viewmodels.TaskViewModel
import java.time.LocalDate

class MainActivity : AppCompatActivity(){

    private lateinit var collapsingToolbarLayout: CollapsingToolbarLayout
    private lateinit var createTaskButton:MaterialButton
    private lateinit var tasksViewModel:TaskViewModel
    private lateinit var tasksRecyclerView: RecyclerView
    private lateinit var tasksAdapter: TasksAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        collapsingToolbarLayout = findViewById(R.id.collapsingToolbar)
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar)
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar)
        collapsingToolbarLayout.title = readableDateFormatter.format(LocalDate.now())

        tasksViewModel = ViewModelProviders.of(
            this@MainActivity,
            Factory((application as Start).tasksRepository, application)
        ).get(TaskViewModel::class.java)

        tasksViewModel.getTodayTasks().observe(this, Observer{
            Log.d("MainActivity", it.toString())
            tasksAdapter.updateList(it)
        })

        tasksRecyclerView = findViewById(R.id.tasksRecyclerView)
        tasksAdapter = TasksAdapter(object:TasksAdapter.TaskAdapterListener{
            override fun taskClicked(id: Long) {
                startTaskActivity(id)
            }

        })
        tasksRecyclerView.adapter = tasksAdapter

        tasksRecyclerView.layoutManager = LinearLayoutManager(this)

        createTaskButton = findViewById(R.id.createTaskButton)
        createTaskButton.setOnClickListener {
            startCreateTaskActivity(0)
        }
    }

    private fun startTaskActivity(id:Long){
        val intent = Intent(this, TaskActivity::class.java)
        intent.putExtra("taskId", id)
        startActivity(intent)
    }

    private fun startCreateTaskActivity(id:Long){
        val intent = Intent(this, CreateTaskActivity::class.java)
        intent.putExtra("taskId", id)
        startActivity(intent)
    }
}
