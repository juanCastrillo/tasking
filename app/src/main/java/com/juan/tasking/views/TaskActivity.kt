package com.juan.tasking.views

import android.graphics.Rect
import android.os.AsyncTask
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.button.MaterialButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.juan.customviews.MaterialLoadingBar
import com.juan.tasking.R
import com.juan.tasking.Start
import com.juan.tasking.extras.readableDateFormatter
import com.juan.tasking.extras.secondsPast
import com.juan.tasking.models.FullTask
import com.juan.tasking.storage.entities.DayWork
import com.juan.tasking.viewmodels.Factory
import com.juan.tasking.viewmodels.TaskViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.LocalDateTime

class TaskActivity:AppCompatActivity(){

    private lateinit var tasksViewModel:TaskViewModel

    private lateinit var taskImage:ImageView
    private lateinit var taskTitle:TextView
    private lateinit var taskDescription:TextView
    private lateinit var dateTextView: TextView
    private lateinit var taskProgressBar: MaterialLoadingBar
    private lateinit var historyBottomSheet:LinearLayout
    private lateinit var floatingHistoryButton:FloatingActionButton

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    val displayRect = Rect()

    private var translateDistance = 0
    private var progress:Long = 0
    private var progressAdded = false

    private var id:Long = 0

    private var working = false

    private lateinit var playPauseButtom: MaterialButton
    private lateinit var task:FullTask
    private var extraProgress:Long = 0

    //private lateinit var progressProcess:AsyncTask<LocalDateTime, Double, Double>

    private var progressProcess:ProgressProcess? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)

        //get id of the task to use
        id = intent.getLongExtra("taskId",0)

        //Initialize the viewmodel
        tasksViewModel = ViewModelProviders.of(
            this,
            Factory((application as Start).tasksRepository, application)
        ).get(TaskViewModel::class.java)

        //Initialize ui components
        taskTitle=findViewById(R.id.task_title)
        taskDescription = findViewById(R.id.task_description)
        dateTextView = findViewById(R.id.todayDate)
        taskProgressBar = findViewById(R.id.task_progressBar)
        taskImage = findViewById(R.id.task_image)
        playPauseButtom = findViewById(R.id.playPauseButton)
        historyBottomSheet = findViewById(R.id.historyBottomSheet)
        floatingHistoryButton = findViewById(R.id.floatingHistoryButton)

        window.decorView.getWindowVisibleDisplayFrame(displayRect)
        historyBottomSheet.layoutParams.height = calculateBottomSheetMaxHeight()
        translateDistance = calculateSize()

        bottomSheetBehavior = BottomSheetBehavior.from(historyBottomSheet)

        Log.d("TaskActivity", "imageHeight: ${taskImage.height}")

        if(id != 0L) tasksViewModel.getTask(id).observe(this, Observer { task = it; updateViewWithTask() })
        dateTextView.text = readableDateFormatter.format(LocalDate.now())



        bottomSheetBehavior.setBottomSheetCallback( object:BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet:View, newState:Int) {
                when(bottomSheetBehavior.state){
                    BottomSheetBehavior.STATE_EXPANDED ->
                        floatingHistoryButton.setImageDrawable(
                            ContextCompat.getDrawable(applicationContext, R.drawable.ic_outline_keyboard_arrow_down_24px)
                        )
                    BottomSheetBehavior.STATE_COLLAPSED -> floatingHistoryButton.setImageDrawable(
                        ContextCompat.getDrawable(applicationContext, R.drawable.ic_outline_keyboard_arrow_up_24px)
                    )
                }

            }

            override fun onSlide(bottomSheet:View, slideOffset:Float) {
                Log.d("slideOffset", slideOffset.toString())
                floatingHistoryButton.animate().translationX(slideOffset*translateDistance).translationY(slideOffset*floatingHistoryButton.height).setDuration(0).start()
            }
        })

        //sets the listener of the button to start and stop when toggle
        playPauseButtom.setOnClickListener {
            if (::task.isInitialized) {
                if (working) stopWorking()
                else startWorking()
            }
        }

        floatingHistoryButton.hideMotionSpec = null
        floatingHistoryButton.setOnClickListener {
            when(bottomSheetBehavior.state){
                BottomSheetBehavior.STATE_EXPANDED -> bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                BottomSheetBehavior.STATE_COLLAPSED -> bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }

    }

    private fun calculateBottomSheetMaxHeight(): Int =
        (displayRect.bottom - resources.getDimension(R.dimen.image_size) - dpiToPixels(24f)).toInt()// - dpiToPixels(56*2f) - 20f - taskImage.height).toInt()

    /**
     * calculates the distance needed to move the fab from the center to the corner
     */
    private fun calculateSize():Int{


        val padding = dpiToPixels(16f)
        val totalPadding = (resources.getDimension(R.dimen.floating_action_buttom)/2).toInt() + padding
        val travelling = displayRect.right/2
        return travelling - totalPadding
    }

    /**
     * Stops the AsyncTask from continuing working
     */
    private fun stopWorking() {

        stopProcess()

        //calculate elapsed time in last progress
        extraProgress = secondsPast(task.dayWork!!.lastTime!!)

        //update task progress and removes the last time cuz its finished
        task.dayWork!!.lastTime = null
        task.dayWork!!.progress += extraProgress

        //resets parameters
        progress = task.dayWork!!.progress
        extraProgress = 0

        //save progress on storage
        updateProgress()
    }

    private fun instanciateProgress(){
        progressProcess = ProgressProcess(obj)
    }

    /**
     * starts the process from scratch from this time
     * and updates it
     */
    private fun startWorking() {


        /*
        If the task doesn't have a day work attached
        Creates a DayWork object and add it to the fulltask
        */
        if(task.dayWork == null) {
            createDayWorkAndUpdate()
            return
        }

        task.dayWork!!.lastTime = LocalDateTime.now()

        //starts the task
        startProcess()

        //update doing things (maybe removed)
        updateProgress()

    }

    /**
     * Once gotten a FullTask the Ui is updated with it
     */
    fun updateViewWithTask() {

        //taskImage.setImageBitmap()
        taskTitle.text = task.task.title
        taskDescription.text = task.task.description

        //Checks if there has been previous progress today
        if (task.dayWork != null) {

            //loads previous progress already saved in storage
            progress = task.dayWork!!.progress

            //Check if the previous progress has been close or if its running still
            //and needs to be continued
            if (task.dayWork!!.lastTime != null) {

                //progress not stoped from the last time it started count to now
                val notPausedBeforeProgress = secondsPast(task.dayWork!!.lastTime!!)
                extraProgress = notPausedBeforeProgress

                //continues the not stopped progress
                startProcess()

            }
        }

        updateProgressBar(extraProgress)
    }


    /**
     * creates a DayWork and updates it into storage
     */
    fun createDayWorkAndUpdate() {

        task.dayWork = DayWork(
            null,
            LocalDate.now(),
            LocalDateTime.now(),
            id!!, //the id of the task in the activity
            0,
            tasksViewModel.isNeccesaryTask(task, LocalDate.now().dayOfWeek)
        )

        GlobalScope.launch(Dispatchers.Default) { tasksViewModel.updateProgress(task.dayWork!!) }
    }

    /**
     * starts the asyncTask
     * and updates ui to show it
     */
    fun startProcess(){

        if(progressProcess != null) stopProcess()
        if(progressProcess == null) instanciateProgress()


        Log.d("isItReallyNull", progressProcess.toString())
        progressProcess!!.execute(task.dayWork!!.lastTime)

        playPauseButtom.setIconResource(R.drawable.ic_outline_pause_circle_outline_24px)

        working = true
    }

    /**
     * stops the asyncTask
     * and updates ui to show it
     */
    fun stopProcess(){

        //ends the running progress
        if(progressProcess!=null)
            progressProcess!!.cancel(true)
        progressProcess = null

        //show in the ui that it has stopped
        playPauseButtom.setIconResource(R.drawable.ic_outline_play_arrow_24px)

        working = false
    }

    /**
     * updates the dayWork Progress into ext
     */
    fun updateProgress(){

        if(::task.isInitialized) {
            if(task.dayWork != null) {
                Log.d("TaskActivity", "dayWork: ${task.dayWork!!}")
                GlobalScope.launch(Dispatchers.Default) { tasksViewModel.updateProgress(task.dayWork!!) }
            }
        }
    }

    /**
     * updates the MaterialLoadingBar parameters to show
     * the progress being made on the task
     */
    fun updateProgressBar(extraProgressTemp:Long){

        extraProgress = extraProgressTemp

        Log.d("TaskActivity", "progress: ${progress+extraProgress} / ${task.task.duration}")

        val percentage = tasksViewModel.getPercentage(task, progress + extraProgress)

        //TODO mandar aviso sobre teminar esto
        if(percentage >= 1.0) {
            taskProgressBar.progressBarColor = ContextCompat.getColor(this, R.color.colorCompletedLoadingBar)
            taskProgressBar.setSmallPercentage(1f)
        }
        else {
            taskProgressBar.progressBarColor = ContextCompat.getColor(this, R.color.colorAccent)
            taskProgressBar.updatePercentage(percentage)
        }


        Log.d("TaskActivity","percentage: $percentage")
    }

    /**
     * @param updateListener methods inside to update ui
     * Async task given the starting date calculates how much time has passed
     * and updates the ui with the time every second (never ends)
     */
    class ProgressProcess(val updateListener:UpdateUI): AsyncTask<LocalDateTime, Long, Long>() {

        override fun doInBackground(vararg params: LocalDateTime?): Long {
            while(true) {
                Thread.sleep(1000)
                val difference = secondsPast(params[0]!!)
                publishProgress(difference)
            }
        }

        override fun onProgressUpdate(vararg values: Long?) { updateListener.updateProgress(values[0]!!) }

    }

    val obj = object :UpdateUI{
        override fun updateProgress(extraProgress: Long) {
            updateProgressBar(extraProgress)
        }
    }

    fun dpiToPixels(dpi:Float):Int{
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dpi,
            resources.displayMetrics
        ).toInt()
    }

    interface UpdateUI{ fun updateProgress(extraProgress: Long) }

    /**
     * Kill all running things and make sure
     * the task is updated before finishing the activity
     */
    override fun onBackPressed() {

        stopProcess()

        //updateProgress()

        finish()
        //super.onBackPressed()

    }
}