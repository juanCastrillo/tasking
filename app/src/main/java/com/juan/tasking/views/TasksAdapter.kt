package com.juan.tasking.views

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import com.juan.tasking.R
import com.juan.tasking.models.FullTask
import com.juan.tasking.storage.entities.Task

class TasksAdapter(private val taskAdapterListener: TaskAdapterListener): RecyclerView.Adapter<TasksAdapter.TaskViewHolder>() {

    private var list = listOf<FullTask>()

    fun updateList(newList: List<FullTask>){
        list = newList
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.taskTitle.text = list[position].task.title
        holder.taskDescription.text = list[position].task.description

        if(list[position].dayWork != null) {
            val progress = list[position].dayWork!!.progress
            holder.taskProgress.text = if (progress == 0L) "not started" else "$progress / ${list[position].task.duration} h"
        }

        try {
            holder.task_card.setOnClickListener { taskAdapterListener.taskClicked(list[position].task.id!!) }
        } catch (e:Exception){ Log.e("taskAdapter", "task id not found")}
    }

    override fun getItemCount(): Int {
        Log.d("TaskAdapter", "listSize: ${list.size}")
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.task_card, parent, false)
        return TaskViewHolder(view)
    }

    class TaskViewHolder(v: View): RecyclerView.ViewHolder(v){
        val task_card: View
        val taskTitle:TextView
        val taskDescription:TextView
        val taskProgress:TextView

        init {
            task_card = v.findViewById(R.id.task_card)
            taskTitle = v.findViewById(R.id.taskTitle)
            taskDescription = v.findViewById(R.id.taskDescription)
            taskProgress = v.findViewById(R.id.taskProgress)
        }
    }

    interface TaskAdapterListener {
        fun taskClicked(id:Long)
    }
}