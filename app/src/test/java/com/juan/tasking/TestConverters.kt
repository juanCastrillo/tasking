package com.juan.tasking

import com.juan.tasking.extras.concatenate
import com.juan.tasking.extras.concatenationToListDayWeek
import com.juan.tasking.extras.toInt
import org.junit.Test

import org.junit.Assert.*
import java.time.DayOfWeek

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TestConverters {

    @Test
    fun DaytoIntTranformation() {

        val day = DayOfWeek.SUNDAY
        val calculatedInt = 6

        val daytoint = day.toInt()

        assertEquals(calculatedInt, daytoint)
    }

    @Test
    fun DaysToListTransformation(){
        val listOfDaysWeek = listOf<DayOfWeek>(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY,
            DayOfWeek.THURSDAY, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY, DayOfWeek.SUNDAY)
        val expectedString = "0123456"

        val getString = listOfDaysWeek.concatenate()

        assertEquals(expectedString, getString)
    }

    @Test
    fun ListToDaysTransformation() {
        val daysString = "0123456"
        val expectedList = listOf<DayOfWeek>(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY,
            DayOfWeek.THURSDAY, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY, DayOfWeek.SUNDAY)

        val getList = concatenationToListDayWeek(daysString)

        assertEquals(expectedList, getList)
    }


}
