package com.juan.tasking

import com.juan.tasking.extras.readableDateFormatter
import org.junit.Test
import java.time.LocalDate
import org.junit.Assert.*

class TestDate {

    @Test
    fun readableDate(){

        val date = LocalDate.of(2019, 1, 20)
        val expectedString = "domingo, 20 enero 2019"

        val resultString = readableDateFormatter.format(date)

        assertEquals(expectedString, resultString)

    }

}